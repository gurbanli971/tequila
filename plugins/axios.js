export default function ({ $axios, error: nuxtError }) {
  $axios.onRequest((config) => {
    config.headers['Content-Type'] = 'application/json'
    config.url.split('/').includes('combinations.pickvisa.com') ? config.headers['Authorization'] = 'Token 08582a913f9d27ba7124645843ec2d3ad121597a' : false
  })


  $axios.onError((error) => {
    const statusCode = error.response?.status
    const message = error.message || 'Server error'

    if(error.response.data.message === 'No combination found') {
      return
    }
    else if ([404,500].includes(statusCode)) {
      nuxtError({ statusCode, message })
    }
  })
}
