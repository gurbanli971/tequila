export const WithdawalMixin = {
  computed: {
    withdrawMethods() {
      return [
        { id: 1, title: 'Payoneer', subTitle: 'Only available in USD', imageSrc: '/images/withdrawal-methods/payoneer.svg' },
        { id: 2, title: 'Bank transfer', subTitle: 'Support 180+ currencies', imageSrc: '/images/withdrawal-methods/bank.svg' },
        { id: 3, title: 'Wise', subTitle: 'Instant and support 7 currencies', imageSrc: '/images/withdrawal-methods/wise.svg' },
        { id: 4, title: 'Paypal', subTitle: 'Only available in USD', imageSrc: '/images/withdrawal-methods/paypal.svg' },
        { id: 5, title: 'Coinbase', subTitle: 'Support 5 cryptocurrencies', imageSrc: '/images/withdrawal-methods/coinbase.svg' },
      ]
    },
  },
}
