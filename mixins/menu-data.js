export const MenuMixin = {
  computed: {
    navItems() {
      return [
        { id: 0, title: 'Personal info', route: '/info', icon: 'grid-7' },
        // { id: 1, title: 'Deeplinks', route: '/deeplinks', icon: 'linkex' },
        { id: 2, title: 'Widgets', route: '/widgets', icon: 'element-1ex' },
        { id: 3, title: 'Payouts', route: '/payouts', icon: 'document-text' },
        { id: 4, title: 'Q&A', route: '/faq', icon: 'document-text' },
      ]
    },
  }
}
