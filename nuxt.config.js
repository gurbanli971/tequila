export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Visaland',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: "apple-touch-icon", sizes:"57x57", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-57x57.png"},
      { rel: "apple-touch-icon", sizes:"60x60", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-60x60.png"},
      { rel: "apple-touch-icon", sizes:"72x72", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-72x72.png"},
      { rel: "apple-touch-icon", sizes:"76x76", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-76x76.png"},
      { rel: "apple-touch-icon", sizes:"114x114", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-114x114.png"},
      { rel: "apple-touch-icon", sizes:"120x120", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-120x120.png"},
      { rel: "apple-touch-icon", sizes:"144x144", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-144x144.png"},
      { rel: "apple-touch-icon", sizes:"152x152", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-152x152.png"},
      { rel: "apple-touch-icon", sizes:"180x180", href:"https://static.pickvisa.com/website/v2/favicons/apple-icon-180x180.png"},
      { rel: "icon", type:"image/png", sizes:"192x192", href:"https://static.pickvisa.com/website/v2/favicons/android-icon-192x192.png"},
      { rel: "icon", type:"image/png", sizes:"32x32", href:"https://static.pickvisa.com/website/v2/favicons/favicon-32x32.png"},
      { rel: "icon", type:"image/png", sizes:"96x96", href:"https://static.pickvisa.com/website/v2/favicons/favicon-96x96.png"},
      { rel: "icon", type:"image/png", sizes:"16x16", href:"https://static.pickvisa.com/website/v2/favicons/favicon-16x16.png"},
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@/assets/scss/style.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/axios' },
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/helpers' },
    { src: '~/plugins/global-components' },
    { src: '~/plugins/vue-datepicker' },
    { src: '~/plugins/vue-select' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // mode: 'universal',
  ssr: false,
  target: 'static',

  router: {
    linkActiveClass: 'link-active',
    linkExactActiveClass: 'exact-active',
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxtjs/toast',
    '@nuxtjs/moment',
  ],
  
  loading: {
    color: '#189ca9',
    height: '3px'
  },

  toast: {
    position: 'top-center',
    keepOnHover: true,
    duration: 3000,
  },

  axios: {
    // Do away with the baseUrl when using proxy
    // proxy: true,
    // baseURL: 'https://api.affiliate.pickvisa.com',
    proxyHeaders: false,
    credentials: false
  },

  proxy: {
    // Simple proxy
    '/api/': {
      target: 'https://api.affiliate.pickvisa.com',
      // pathRewrite: { '^api/': '' },
      changeOrigin: true
    },
    '/api2/': {
      target: 'https://combinations.pickvisa.com',
      pathRewrite: { '/api2/': '/api/' },
      changeOrigin: true
    },
  },

  auth: {
    localStorage: true,
    strategies: {
      local: {
        scheme: 'refresh',
        token: {
          property: 'result.access',
          global: true,
          required: true,
          maxAge: 1800,
        },
        refreshToken: {
          property: 'result.refresh',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'result',
          autoFetch: true,
        },
        endpoints: {
          login: { url: 'https://api.affiliate.pickvisa.com/api/v1/users/signin/', method: 'post' },
          logout: false,
          user: { url: 'https://api.affiliate.pickvisa.com/api/v1/users/detail/', method: 'get' },
        },
      },
    },
    redirect: {
      login: '/login',
      logout: '/login',
      callback: '/login',
      home: '/info',
    },
  },

  render: {
    bundleRenderer: {
      shouldPreload: (file, type) => {
        if (type === 'script' || type === 'style') {
          return true
        }
        if (type === 'font') {
          // only preload woff2 fonts
          return /\.woff2$/.test(file)
        }
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
