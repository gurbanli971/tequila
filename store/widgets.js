export const state = () => ({
  widgets: [],
  widgetByTrackId: {},
  services: {}
})

export const getters = {
  widgets: (s) => s.widgets,
  widgetByTrackId: (s) => s.widgetByTrackId,
  services: (s) => s.services?.visa_types?.map(service => {
    const subs = service.sub_types.map(item => {
      return {
        subId: item.id,
        serviceTitle: service.title,
        subTitle: item.title,
        totalPrice: item.total_fee_price,
        currency: item.total_fee_currency
      }
    });
    return {
      serviceId: service.id, 
      sub_types: subs,
      title: service.title
    }
  }),
}

export const mutations = {
  SET_WIDGETS(state, { widgets }) {
    state.widgets = widgets.result
  },
  SET_WIDGET_BY_TRACK_ID(state, { widget }) {
    state.widgetByTrackId = widget.result
  },
  SET_SERVICES(state,{ services }) {
    state.services = services
  }
}

export const actions = {
  async fetchWidgets({ commit }) {
    const { data: widgets } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/widgets/')
    commit('SET_WIDGETS', { widgets })
  },
  async fetchWidgetByTrackId({ commit,state }, track_id) {
    if(state.widgetByTrackId?.track_id === track_id)
      return
    const { data: widget } = await this.$axios.get(`https://api.affiliate.pickvisa.com/api/v1/widgets/${track_id}/`)
    commit('SET_WIDGET_BY_TRACK_ID', { widget })
  },
  async createWidget({ dispatch },form) {
    try {
      const response = await this.$axios.post('https://api.affiliate.pickvisa.com/api/v1/widgets/',form)
      this.$toast.success('Widget created succesfully')
      this.$router.push('/widgets')
    }catch(e) {
      this.$toast.error(e)
    }
  },
  async fetchServicePrices({ commit },{citizen_of,resident_of,travel_to}) {
    const { data: services } = await this.$axios.get(`https://api.affiliate.pickvisa.com/api/v1/combinations/filter/?travel_to=${travel_to}&citizen_of=${citizen_of}&resident_of=${resident_of}&currency=EUR`)
    commit('SET_SERVICES', { services })
},
  async updateWidget({ dispatch },{form, track_id }) {
    try {
      const response = await this.$axios.put(`https://api.affiliate.pickvisa.com/api/v1/widgets/${track_id}/`,form)
      this.$toast.success('Widget updated succesfully')
      this.$router.push('/widgets')
    }catch(e) {
      this.$toast.error(e)
    }
  },
}
