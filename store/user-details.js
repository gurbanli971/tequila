export const state = () => ({
  userDetails: {},
})

export const getters = {
  userDetails: (s) => s.userDetails,
}

export const mutations = {
  SET_DETAILS(state, { details }) {
    state.userDetails = details.result
  },
}

export const actions = {
  async fetchUserDetails({ commit }) {
    const { data: details } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/users/detail/')
    commit('SET_DETAILS', { details })
  },
  async updateProfileInfo({ commit },form) {
    try {
      const response = await this.$axios.put(`https://api.affiliate.pickvisa.com/api/v1/users/update_profile/`,form)
      if(response.data.success) {
        this.$toast.success('Successfully updated')
        this.$router.push('/info')
      }else {
        this.$toast.error(response.data.result)
      }
    } catch (err) {
      this.$toast.error(err.response.data.result)
    }
  },
  async updateCompanyInfo({ commit },form) {
    try {
      const response = await this.$axios.put(`https://api.affiliate.pickvisa.com/api/v1/users/update_company/`,form)
      if(response.data.success) {
        this.$toast.success('Successfully updated')

        this.$router.push('/info')
      }else {
        this.$toast.error(response.data.result)
      }

    } catch (err) {
      this.$toast.error(err.response.data.result)
    }
  },
}