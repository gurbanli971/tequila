export const state = () => ({
  deeplinks: [],
  services: [],
  deeplinkById: {}
})

export const getters = {
  deeplinks: (s) => s.deeplinks,
  deeplinkById: (s) => s.deeplinkById,
  services: (s) => s.services,
  serviceOptions: (s) => s.services?.map((obj) => ({ label: obj.title, value: obj.id })),
}

export const mutations = {
  SET_DEEPLINKS(state, { deeplinks }) {
    state.deeplinks = deeplinks.result
  },
  SET_DEEPLINK_BY_ID(state, { deeplink }) {
    state.deeplinkById = deeplink.result
  },
  SET_SERVICES(state, { services }) {
    state.services = services.visa_types
  },
}

export const actions = {
  async fetchDeeplinks({ commit }) {
    const { data: deeplinks } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/deeplinks/')
    commit('SET_DEEPLINKS', { deeplinks })
  },
  async fetchDeeplinkById({ commit }, track_id) {
    const { data: deeplink } = await this.$axios.get(`https://api.affiliate.pickvisa.com/api/v1/deeplinks/${track_id}`)
    commit('SET_DEEPLINK_BY_ID', { deeplink })
  },
  async fetchServices({ commit },{citizen_of,resident_of,travel_to}) {
      const { data: services } = await this.$axios.get(`https://combinations.pickvisa.com/api/v1/combinations/filter?travel_to=${travel_to}&citizen_of=${citizen_of}&resident_of=${resident_of}`)
      commit('SET_SERVICES', { services })
      this.$toast.success('Success')
  },

  async createDeeplink({ dispatch },form) {
    await this.$axios.post('https://api.affiliate.pickvisa.com/api/v1/deeplinks/',form)
    // dispatch('fetchDeeplinks')
    this.$toast.success('Deeplink created successfully')
    this.$router.push('/deeplinks')
  },
}
