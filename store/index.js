export const state = () => ({
  user: {
    id: '',
    is_company: false
  },
  countries: [],
})

export const getters = {
  countries: (s) => s.countries,
  countrySlug: (s) => (alpha) => s.countries.find(country => country.alpha_3_code === alpha).slug,
  countriesOptions: (s) => s.countries?.map((obj) => ({ label: obj.name, value: obj.alpha_3_code })),
  isCompany: (s) => s.user.is_company,
}

export const mutations = {
  SET_USER(state, { id, is_company }) {
    state.user.id = id
    state.user.is_company = is_company
  },
  SET_COUNTRIES(state, { countries }) {
    state.countries = countries.data
  },
}

export const actions = {
  async fetchCountries({ commit,state }) {
    if(this.state.countries.length)
      return
    const { data: countries } = await this.$axios.get('https://ace.pickvisa.com/api/v1/countries/all?with_continent=0')
    commit('SET_COUNTRIES', { countries })
  }
}