export const state = () => ({
  payoutRequests: [],
  balance: {
    total: '',
    current: ''
  },
  meta: {
    count: '',
    next: null,
    previous: null
  },
  providers: []
})

export const getters = {
  payoutRequests: (s) => s.payoutRequests,
  balance: (s) => s.balance,
  providers: (s) => s.providers,
}

export const mutations = {
  SET_USER_BALANCE(state, { balance }) {
    state.balance.total = balance.result.total
    state.balance.current = balance.result.current_amount
  },
  SET_PAYOUT_REQUESTS(state, { requests }) {
    state.payoutRequests = requests.results
  },
  SET_PAYOUT_REQUESTS_META(state, { requests }) {
    state.meta.count = requests.count
    state.meta.next = requests.next
    state.meta.previous = requests.previous
  },
  SET_PAYOUT_PROVIDERS(state, { providers }) {
    state.providers = providers.result
  },
}

export const actions = {
  async fetchPayoutRequestsList({ commit }) {
    const { data: requests } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/users/payout_requests/')
    commit('SET_PAYOUT_REQUESTS', { requests })
    commit('SET_PAYOUT_REQUESTS_META', { requests })
  },
  async fetchUserBalance({ commit }) {
    const { data: balance } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/users/balance/')
    commit('SET_USER_BALANCE', { balance })
  },
  async fetchProviders({ commit }) {
    const { data: providers } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/users/payout_providers/')
    commit('SET_PAYOUT_PROVIDERS', { providers })
  },
  async withdrawMoney({ dispatch }, body = { amount, provider_id }) {
    try {
      await this.$axios.post('https://api.affiliate.pickvisa.com/api/v1/users/payout_request/', body)
      await dispatch('fetchUserBalance')
      this.$toast.success('Withdrawal succesfull')
    }catch(error) {
      console.log(error);
    }
  },
  async withdrawCancel({ dispatch }, id) {  
    try {
      await this.$axios.post(`https://api.affiliate.pickvisa.com/api/v1/users/cancel_payout/${id}/`)
      dispatch('fetchPayoutRequestsList')
      this.$toast.success('Operation cancelled succesfully')
    }catch(error) {
      console.log(error);
    }
  }
}