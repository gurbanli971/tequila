export const state = () => ({
  faqs: [],
})

export const getters = {
  faqs: (s) => s.faqs,
}

export const mutations = {
  SET_FAQS(state, { faqs }) {
    state.faqs = faqs.results
  },
}

export const actions = {
  async fetchFaqs({ commit,state }) {
    const { data: faqs } = await this.$axios.get('https://api.affiliate.pickvisa.com/api/v1/content/faq/')
    commit('SET_FAQS', { faqs })
  }
}